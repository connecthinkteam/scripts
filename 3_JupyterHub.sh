# AUTHOR: XXA DATE: 17-NOV-2019

#Install NodeJS
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get -y install nodejs

#Install Java Runtime Environment
sudo apt-get -y install default-jre

#Install Python 3
apt-get -y install python3-pip

#Install configurable-http-proxy
npm install -g configurable-http-proxy

#Install Optimus & Spark
pip3 install optimuspyspark

#Install JupyterHub
pip3 install jupyterhub

#Upgrade notebook
pip3 install --upgrade notebook

#Generate Config File
mkdir /etc/jupyterhub

mv jupyterhub_config.py /etc/jupyterhub/jupyterhub_config.py

#Run Jupyterhub
# jupyterhub --config /etc/jupyterhub/jupyterhub_config.py