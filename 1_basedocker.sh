# AUTHOR: XXA DATE: 19-FEB-2020

#PREVIOUS: 
# apt-get install git
# git clone https://ConnecThink@bitbucket.org/connecthinkteam/scripts.git
#
# EXECUTE:
# bash -i 1_basedocker.sh


# quietly add a user without password
adduser --quiet --disabled-password --shell /bin/bash --home /home/ctadmin --gecos "User" ctadmin

# set password
echo "ctadmin:ctadmin@2020" | chpasswd

#set sudoer
usermod -aG sudo ctadmin

# quietly add a user without password for neo
adduser --quiet --disabled-password --shell /bin/bash --home /home/neo4j --gecos "User" neo4j

# set password for neo
echo "neo4j:ctadmin@2019" | chpasswd

#set sudoer for neo
usermod -aG sudo neo4j

# copiamos la configuracion que deshabilita login de root
mv ./config/sshd_config /etc/ssh/sshd_config
sudo systemctl reload sshd


# set firewall minimum config & neo config
ufw allow OpenSSH
ufw allow 80
ufw allow 443
ufw allow 7474
ufw allow 7687
ufw enable

#install docker engine
# based on: https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es

sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce

sudo apt install docker-ce
sudo systemctl enable docker

#post actions based on:https://docs.docker.com/install/linux/linux-postinstall/

sudo groupadd docker
sudo usermod -aG docker ctadmin
sudo usermod -aG docker root
sudo usermod -aG docker neo4j


