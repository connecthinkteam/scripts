# AUTHOR: XXA DATE: 17-NOV-2019

# PREREQUISITES: conda init bash

# EXECUTION: bash -i file.sh

# OR EXECUTION: source file.sh

conda create --name ct_nlp

conda create --name ct_etl

conda create --name ct_dcs
