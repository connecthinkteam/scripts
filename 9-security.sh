# AUTHOR: XXA DATE: MAY-2020
# sudo bash -i 9-security.sh 

# Limit SSH to only ctadmin
# without refusing other intents
# ref: https://www.lifewire.com/harden-ubuntu-server-security-4178243

echo "AllowUsers ctadmin@*.*.*.* " >> /etc/ssh/sshd_config
sudo systemctl restart sshd

#Memory atack limit

echo "tmpfs /run/shm tmpfs defaults,noexec,nosuid 0 0" >> /etc/fstab

#Limit Sudoers

sudo groupadd admin
sudo usermod -a -G admin ctadmin
sudo dpkg-statoverride --update --add root admin 4750 /bin/su

#install fail2ban

sudo apt-get install fail2ban
mv ./config/jail.local /etc/fail2ban/jail.local
sudo systemctl restart fail2ban

#reboot to save changes


sudo reboot

