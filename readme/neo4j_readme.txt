# AUTHOR: XXA DATE: 19-FEB-2020

Command for executing neo4J on the folder /HOME/neo4j/data

docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    neo4j

#problem: El neo4j supone la existencia del usuario neo4j, y trata de acceder al VOLUMEN con este usuario. Por eso en el server hay un usuario neo4j, para evitar este conflicto

which allows you to access neo4j through your browser at http://localhost:7474.

This binds two ports (7474 and 7687) for HTTP and Bolt access to the Neo4j API. A volume is bound to /data to allow the database to be persisted outside the container.

By default, this requires you to login with neo4j/neo4j and change the password. You can, for development purposes, disable authentication by passing --env=NEO4J_AUTH=none to docker run.

#LINUX SERVER 



Utilitzar el current user quan Docker es comuniqui amb el Host i guardar tambe un directory per fer imports

-d para detached execution (en background)

sudo chmod 777 neo4j
cd neo4j
sudo chmod 777 conf

sudo docker run --rm \
--privileged \
--volume=$HOME/neo4j/conf:/conf \
neo4j dump-config

Aixo permet crear un fitxer de condiguracio que podem editar.

Cal asegurar el access desde fora amb:
dbms.connector.bolt.address=0.0.0.0:7687

Despres ja podrem accedir


docker run \
--detach \
--publish=7474:7474 --publish=7687:7687 \
--user="$(id -u):$(id -g)" \
--volume=$HOME/neo4j/data:/data \
--volume=$HOME/neo4j/logs:/logs \
--volume=$HOME/neo4j/import:/import \
--volume=$HOME/neo4j/conf:/conf \
--volume=$HOME/neo4j/plugins:/plugins \
--env NEO4J_dbms_connector_bolt_address=0.0.0.0:7687 \
--env NEO4J_dbms_connector_bolt_tls_level=OPTIONAL \
neo4j 

# IMPORTANT

Quan accedir desde el browser hem de posar bolt://88.99.224.154:7687 abans de fer login o no funciona el Neo4j


#EXAMPLE2

Canviar el password de neo4j per defecte i guardar tambe els logs

(per ara no em funciona per el neo4J del HOST no te permisos per executar el docker...


docker run \
--detach \
--publish=7474:7474 --publish=7687:7687 \
--volume=$HOME/neo4j/data:/data \
--volume=$HOME/neo4j/logs:/logs \
--volume=$HOME/neo4j/import:/import \
--env NEO4J_AUTH=neo4j/ctadmin@2019\
    neo4j


#LOCAL SERVER (mac tested)

docker run \
--detach \
--publish=7474:7474 --publish=7687:7687 \
--user="$(id -u):$(id -g)" \
--volume=$HOME/neo4j/data:/data \
--volume=$HOME/neo4j/logs:/logs \
--volume=$HOME/neo4j/import:/import \
--volume=$HOME/neo4j/conf:/conf \
neo4j 


