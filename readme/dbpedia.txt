# TRAEMOS LA IMAGEN

docker pull dbpedia/spotlight-spanish

# abrimos firewall

ufw allow 2231

# Ejecutamos y la ponemos en el puerto 2231 del host

docker run -d -p 2231:80 dbpedia/spotlight-spanish spotlight.sh

Ejemplos de llamada:

curl http://88.198.218.76:2231/rest/annotate  \
  --data-urlencode "text=No se si este texto funciona en barcelona o no funciona para detectar personas como pedro sanchez"\
  --data "confidence=0.35" \
  -H "Accept: application/json"







