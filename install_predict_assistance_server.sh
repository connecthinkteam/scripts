#!/bin/bash

# This file must be used with "source install_server.sh" *from bash*
# you cannot run it directly

esc="\033"
end="$esc[0m"
title_style="$esc[1m"
title_success="$esc[42m"
line_clear="\r$esc[K"

os_dependencies_title="Installing OS dependencies..."
finished_os_dependencies_title="Installed OS dependencies"
nginx_title="Setting up Nginx..."
finished_nginx_title="Set up Nginx"
certbot_title="Installing Certbot..."
finished_certbot_title="Installed Certbot"
python_environment_title="Setting up the Python virtual environment..."
finished_python_environment_title="Set up the Python virtual environment"
postgresql_title="Setting up the PostgreSQL database..."
finished_postgresql_title="Set up the PostgreSQL database"
django_title="Setting up Django..."
finished_django_title="Set up Django"
gunicorn_title="Creating a screen with gunicorn in it..."
finished_gunicorn_title="Gunicorn screen created (screen -r gunicorn to see it)"

server_user=$USER
server_cores=$(nproc)
gunicorn_workers=$(( $server_cores * 2 + 1 ))
pwd=$(pwd | sed 's_/_\\/_g')

# Sudo check
sudo -v > /dev/null 2>&1

# Ask the user for "sensible" information that shouldn't be stored in the script
echo -n "Enter server domain: "
read server_domain
echo -n "Enter Certbot notification email: "
read user_email
echo -n "Enter database name (must be the same as the Django project name): "
read database_name
echo -n "Enter database password: "
read -s database_password
echo

# Create the install log
cp install.log install.log.old > /dev/null 2>&1
rm install.log > /dev/null 2>&1
touch install.log > /dev/null 2>&1

# Install OS dependencies
echo
echo -e "${title_style}${os_dependencies_title}$end"
sudo apt-get update >> ./install.log
sudo bash utility/install_os_dependencies.sh install >> ./install.log
echo -e "${title_success}${finished_os_dependencies_title}$end"


# Nginx setup
echo
echo -e "${title_style}${nginx_title}$end"
sudo service nginx start >> ./install.log
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.old >> ./install.log
sudo cp ./utility/nginx/nginx.conf /etc/nginx/nginx.conf >> ./install.log
sudo mv /etc/nginx/conf.d/virtual.conf /etc/nginx/conf.d/virtual.conf.old >> ./install.log 2>&1
sudo cp ./utility/nginx/virtual.conf /etc/nginx/conf.d/virtual.conf >> ./install.log
sudo sed -i "s/<pwd>/${pwd}/g" /etc/nginx/conf.d/virtual.conf >> ./install.log
sudo sed -i "s/<server_name>/${server_domain}/g" /etc/nginx/conf.d/virtual.conf >> ./install.log
sudo service nginx restart >> ./install.log
echo -e "${title_success}${finished_nginx_title}$end"


# Certbot install
echo
echo -e "${title_style}${certbot_title}$end"
sudo apt-get install -y software-properties-common >> ./install.log
sudo add-apt-repository -y universe >> ./install.log
sudo add-apt-repository -y ppa:certbot/certbot >> ./install.log
sudo apt-get update >> ./install.log
sudo apt-get install -y certbot python-certbot-nginx >> ./install.log
sudo certbot --nginx --non-interactive --redirect --agree-tos --email $user_email --domain $server_domain >> ./install.log
echo -e "${title_success}${finished_certbot_title}$end"


# Firewall setup (Ubuntu)
sudo ufw allow 'Nginx Full' >> ./install.log


# PostgreSQL setup
echo
echo -e "${title_style}${postgresql_title}$end"
sudo service postgresql start >> ./install.log
sudo -u postgres -H sh -c "psql -c \"CREATE DATABASE $database_name\"" >> ./install.log
sudo -u postgres -H sh -c "psql -c \"CREATE USER $server_user WITH PASSWORD '${database_password}'\"" >> ./install.log
sudo -u postgres -H sh -c "psql -c \"ALTER ROLE $server_user SET client_encoding TO 'utf8'\"" >> ./install.log
sudo -u postgres -H sh -c "psql -c \"ALTER ROLE $server_user SET default_transaction_isolation TO 'read committed'\"" >> ./install.log
sudo -u postgres -H sh -c "psql -c \"ALTER ROLE $server_user SET timezone TO 'Europe/Madrid'\"" >> ./install.log
sudo -u postgres -H sh -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE $database_name TO $server_user\"" >> ./install.log
echo -e "${title_success}${finished_postgresql_title}$end"


# Install Python environment with pip requirements
echo
echo -e "${title_style}${python_environment_title}$end"
deactivate > /dev/null 2>&1
virtualenv -p python3 env >> ./install.log
cp ./utility/environment/postactivate ./env/bin/postactivate
sed -i "s/<server_domain>/${server_domain}/g" ./env/bin/postactivate >> ./install.log
sed -i "s/<database_username>/${server_user}/g" ./env/bin/postactivate >> ./install.log
sed -i "s/<database_password>/${database_password}/g" ./env/bin/postactivate >> ./install.log
sed -i "s/<database_name>/${database_name}/g" ./env/bin/postactivate >> ./install.log
echo -e "\nsource postactivate" >> ./env/bin/activate
source env/bin/activate
pip3 install -r requirements/production.txt >> ./install.log
echo -e "${title_success}${finished_python_environment_title}$end"


# Django init
echo
echo -e "${title_style}${django_title}$end"
sed -i "s/#<server_domain>/\"${server_domain}\"/g" ./config/settings/production.py >> ./install.log
python3 manage.py migrate >> ./install.log
python3 manage.py collectstatic --no-input >> ./install.log 2>&1
#python3 manage.py runserver
echo -e "${title_success}${finished_django_title}$end"


# Gunicorn init
echo
echo -e "${title_style}${gunicorn_title}$end"
screen -dmS gunicorn gunicorn --bind 127.0.0.1:8000 --workers $gunicorn_workers --reload config.wsgi:application >> ./install.log
echo -e "${title_success}${finished_gunicorn_title}$end"