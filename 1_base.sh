# AUTHOR: XXA DATE: 17-NOV-2019

#PREVIOUS: 
# apt-get install git
# git clone https://ConnecThink@bitbucket.org/connecthinkteam/scripts.git
#
# EXECUTE:
# bash -i 1_base.sh


# Quietly add a user without password
adduser --quiet --disabled-password --shell /bin/bash --home /home/ctadmin --gecos "User" ctadmin

# Set password
echo "ctadmin:ctadmin@2020" | chpasswd

# Set sudoer
usermod -aG sudo ctadmin

# Deshabilitamos el login del usuario root por SSH
sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
sudo systemctl reload sshd


# set firewall minimum config
ufw allow OpenSSH
ufw allow 80
ufw allow 443
ufw enable