#Create init file
mv jupyterhub_init /etc/init.d/jupyterhub

#Create service
mv jupyter_service cd /etc/systemd/system/


#Enable Autoboot service
systemctl enable jupyterhub

#Start Service
systemctl start jupyterhub