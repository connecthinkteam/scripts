# AUTHOR: XXA DATE: 19-NOV-2019

# IMPORTANTE: UBUNTU 16.04!!!!
# ####

# EXECUTE:
# bash -i 1_neo4j.sh

# install Java 8 package previous to neo4j
add-apt-repository -y ppa:openjdk-r/ppa
apt-get update

update-java-alternatives --jre --set java-1.8.0-openjdk-amd64 


# add key to package manager
wget -O - http://debian.neo4j.org/neotechnology.gpg.key | apt-key add -

# add neo4j to apt source list
echo 'deb http://debian.neo4j.org/repo stable/' > /etc/apt/sources.list.d/neo4j.list

apt-get update

apt-get install neo4j

ufw allow 7474
ufw enable

neo4j start

#post: check the service
# neo4j status

#for network problems
# nano /etc/neo4j/neo4j.conf
# uncomment  #dbms.connectors.default_listen_address=0.0.0.0

#for login problems
# 
# rm -f /var/lib/neo4j/data/dbms/auth
# neo4j-admin set-initial-password "neo4j"
# neo4j restart

# for log
# nano /var/log/neo4j/neo4j.log
